﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// Управление яблоней
/// </summary>
public class AppleTree : MonoBehaviour
{
    [Header("Префаб яблок")]
    [SerializeField] private GameObject applePrefab;

    [Header("Скорость яблок")]
    [SerializeField] private float speed = 1f;

    [Header("Расстояние, для изменения движения яблони")]
    [SerializeField] private float leftAndRightEdge = 10f;

    [Header("Вероятность случайного изменения направления движения")]
    [SerializeField] private float chanceToChangeDirections = 0.1f;

    [Header("Частота создания яблок")]
    [SerializeField] private float secondsBetweenAppleDrops = 1f;

    [Header("Частота смены направления")]
    [SerializeField] private float secondsChangeDirections = 1f;


    private void Start()
    {
        StartCoroutine(ChangeDirections());
        StartCoroutine(DropApple());
    }

    private IEnumerator DropApple()
    {
        yield return new WaitForSeconds(secondsBetweenAppleDrops);
        
        GameObject apple = Instantiate(applePrefab);
        apple.transform.position = transform.position;

        StartCoroutine(DropApple());
    }

    private void Update()
    {
        transform.position += new Vector3(speed * Time.deltaTime, 0f, 0f);

        if (transform.position.x < -leftAndRightEdge)
        {
            speed = Mathf.Abs(speed);
        }
        else if (transform.position.x > leftAndRightEdge)
        {
            speed = -Mathf.Abs(speed);
        }
    }

    private IEnumerator ChangeDirections()
    {
        while(true)
        {
            if (Random.value < chanceToChangeDirections)
            {
                speed = -speed;
            }

            yield return new WaitForSeconds(secondsChangeDirections);
        }
    }
}
