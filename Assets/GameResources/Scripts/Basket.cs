﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
/// <summary>
/// Управление корзиной
/// </summary>
public class Basket : MonoBehaviour
{
    [Header("Скорость перемещения")]
    [SerializeField] private float speed = 20f;

    [Header("Максимальный отступ от середины")]
    [SerializeField] private float maxDelay = 20f;


    private void Update()
    {
        if(Input.GetKey(KeyCode.LeftArrow))
        {
            if (transform.position.x - speed * Time.deltaTime >= -maxDelay)
            {
                transform.position += new Vector3(-speed * Time.deltaTime, 0f, 0f);
            }
        }

        if (Input.GetKey(KeyCode.RightArrow))
        {
            if (transform.position.x + speed * Time.deltaTime <= maxDelay)
            {
                transform.position += new Vector3(speed * Time.deltaTime, 0f, 0f);
            }
        }
    }


}
