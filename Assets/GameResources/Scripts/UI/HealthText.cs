﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// Текст с жизнью
/// </summary>
public class HealthText : AbstractGameText
{
    private const string HEALTH = "Health: ";

    private void Start()
    {
        UpdateText(gameController.Health);
        gameController.onUpdateHealth += UpdateText;
    }

    private void OnDestroy()
    {
        gameController.onUpdateHealth -= UpdateText;
    }

    private void UpdateText(int health)
    {
        text.text = HEALTH + health.ToString();
    }
}
