﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

/// <summary>
/// Игровой счет
/// </summary>
public class ScoreGame : AbstractGameText
{
    private const string SCORE = "Score: ";

    private void Start()
    {
        UpdateScore(0);
        gameController.onUpdateGameScore += UpdateScore;
    }

    private void OnDestroy()
    {
        gameController.onUpdateGameScore -= UpdateScore;
    }

    private void UpdateScore(int score)
    {
        text.text = SCORE + score.ToString();
    }
}
