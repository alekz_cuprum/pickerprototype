﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

/// <summary>
/// Максимальный счет
/// </summary>
public class HighScore : AbstractGameText
{
    private const string HIGH_SCORE = "High Score: ";

    private void Start()
    {
        UpdateScore();
        gameController.onUpdateHighScore += UpdateScore;
    }

    private void OnDestroy()
    {
        gameController.onUpdateHighScore -= UpdateScore;
    }

    private void UpdateScore()
    {
        text.text = HIGH_SCORE + gameController.HighScore.ToString();
    }
}
