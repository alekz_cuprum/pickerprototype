﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

/// <summary>
/// Абстрактный класс игрового текста
/// </summary>
[RequireComponent(typeof(Text))]
public class AbstractGameText : MonoBehaviour
{
    [SerializeField] protected GameController gameController;

    protected Text text;

    protected void Awake()
    {
        text = GetComponent<Text>();
    }
}
