﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameOverPanel : MonoBehaviour
{
    [SerializeField] private GameObject panel;
    [SerializeField] private GameController gameController;

    private void Start()
    {
        gameController.onGameOver += OpenPanel;
    }

    private void OnDestroy()
    {
        gameController.onGameOver -= OpenPanel;
    }

    private void OpenPanel()
    {
        panel.SetActive(true);
    }
}
