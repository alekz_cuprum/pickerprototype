﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
using UnityEngine.SceneManagement;
/// <summary>
/// Управление игрой
/// </summary>
public class GameController : MonoBehaviour
{
    [Header("Игровые жизни"),Range(1,5)]
    [SerializeField] private int health = 3;
    public int Health => health;

    private const string HIGH_SCORE = "HighScore";

    /// <summary>
    /// Максимальный счет
    /// </summary>
    public int HighScore => highScore;
    private int highScore;

    private int score;

    /// <summary>
    /// Ивент обновления максимального счета
    /// </summary>
    public event Action onUpdateHighScore = delegate { };

    /// <summary>
    /// Ивент обновления игрового счета
    /// </summary>
    public event Action<int> onUpdateGameScore = delegate { };

    /// <summary>
    /// Ивент обновление количества жизней
    /// </summary>
    public event Action<int> onUpdateHealth = delegate { };

    /// <summary>
    /// Ивент окончания игры
    /// </summary>
    public event Action onGameOver = delegate { };

    private void Awake()
    {
        highScore = PlayerPrefs.GetInt(HIGH_SCORE, 0);

        score = 0;
    }

    /// <summary>
    /// Сохранить максимальный счет
    /// </summary>
    public void SaveHighScore(int newScore)
    {
        highScore = newScore;

        PlayerPrefs.SetInt(HIGH_SCORE, highScore);
        PlayerPrefs.Save();

        onUpdateHighScore.Invoke();
    }

    /// <summary>
    /// Обновление счета
    /// </summary>
    public void UpdateScore(int scoreInObject)
    {
        score += scoreInObject;

        onUpdateGameScore.Invoke(score);

        if (score > highScore)
        {
            SaveHighScore(score);
        }
    }

    /// <summary>
    /// Подписать спавнившиеся объект к обновлению счета
    /// </summary>
    public void SubscribeObjectInGame(Apple apple)
    {
        apple.onCollideInBasket += UpdateScore;
        apple.onMissObject += MinusHealth;
    }

    /// <summary>
    /// Отписка объекта
    /// </summary>
    public void UnsubscribeObjectInGame(Apple apple)
    {
        apple.onCollideInBasket -= UpdateScore;
        apple.onMissObject -= MinusHealth;
    }

    /// <summary>
    /// Отнятие жизни
    /// </summary>
    public void MinusHealth()
    {
        health--;

        onUpdateHealth.Invoke(health);

        if (health <= 0)
        {
            GameOver();
        }
    }

    /// <summary>
    /// Конец игры
    /// </summary>
    public void GameOver()
    {
        onGameOver.Invoke();
        Time.timeScale = 0f;
    }

    /// <summary>
    /// Рестарт игры
    /// </summary>
    public void RestartGame()
    {
        Time.timeScale = 1f;
        SceneManager.LoadScene(SceneManager.GetActiveScene().name);
    }

    /// <summary>
    /// Выход из игры
    /// </summary>
    public void QuitGame()
    {
        Application.Quit();
    }

}
