﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
/// <summary>
/// Управление яблоками
/// </summary>
public class Apple : MonoBehaviour
{
    [SerializeField] private int addScore = 100;

    private const string BASKET = "Basket";

    /// <summary>
    /// Ивент столкновения с сеткой
    /// </summary>
    public Action<int> onCollideInBasket = delegate { };

    /// <summary>
    /// Ивент пропуска объекта
    /// </summary>
    public Action onMissObject = delegate { };

    public static float bottomY = -20f;

    private GameController gameController;
    private void Start()
    {
        gameController = FindObjectOfType<GameController>();
        gameController.SubscribeObjectInGame(this);
    }

    private void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject.tag == BASKET)
        {
            onCollideInBasket.Invoke(addScore);
            DestroyObject();
        }
    }

    private void Update()
    {
        if (transform.position.y < bottomY)
        {
            onMissObject.Invoke();
            DestroyObject();
        }
    }

    private void DestroyObject()
    {
        gameController.UnsubscribeObjectInGame(this);
        Destroy(gameObject);
    }
}
